<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_LIB_PATH.'/thumbnail.lib.php');
$thumb1_width=130;
$thumb1_height=80;

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨

// 외부페이지에서는 html_process가 돌아가지 않음. - Jiho
// add_stylesheet('<link rel="stylesheet" href="'.$latest_skin_url.'/style.css">', 0);
?>

<div class="section_vert">
<div class="row main">

  <?php
  $thumb = get_list_thumbnail($bo_table, $list[0]['wr_id'], $thumb1_width, $thumb1_height);
  if($thumb['src']) {
    $img = '<img class="img-responsive center" src="'.$thumb['src'].'" alt="'.$list[0]['subject'].'">'; ?>
    <div class="col-md-4" style="padding-right:0px;">
      <a href="<?php echo $list[0]['href'];?>"><?php echo $img;?></a>
    </div>
    <div class="col-md-8 txt_main">
      <span class="glyphicon glyphicon-leaf"></span>
      <a href="<?php echo $list[0]['href'];?>"><?php echo cut_str( $list[0]['subject'], 18);?></a>
      <span class="cmt"><span class="ico_heart">♥</span><?php echo $list[0]['wr_comment'];?></span>
      <p class="desc_main"><?php echo cut_str(strip_tags($list[0]['wr_content']),80);?></p>
    </div>
  <?php } else { ?>
    <div class="txt_main" style="padding: 5px 15px;">
      <span class="glyphicon glyphicon-leaf"></span>
      <a href="<?php echo $list[0]['href'];?>"><?php echo $list[0]['subject'];?></a>
      <span class="cmt"><span class="ico_heart">♥</span><?php echo $list[0]['wr_comment'];?></span>
      <p class="desc_main" style="margin-bottom:3px;"><?php echo cut_str(strip_tags($list[0]['wr_content']),110);?></p>
    </div>
  <?php } ?>

</div>

<div class="row sub">
  <ul>
    <?php for ($i = 1; $i < count($list); $i++) { ?>
    <li>
      <span class="glyphicon glyphicon-leaf"></span>
      <a href="<?php echo $list[$i]['href'];?>"><?php echo cut_str($list[$i]['wr_subject'],50);?></a>
      <span class="stamp">[<?php echo $list[$i]['wr_name'] . ', ' . $list[$i]['last']; ?>]</span>
    </li>
    <?php } ?>

  </ul>
</div>
</div>
