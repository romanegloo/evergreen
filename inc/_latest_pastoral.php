<?php 
if( !isset( $wpdb ) ) require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php' ); 
$numRows = 5;
$args=array(
  'post_type' => 'post',
  'category_name' => 'pastoralcol',
  'post_status' => 'publish',
  'posts_per_page' => $numRows,
  'ignore_sticky_posts' => 1,
  'orderby' => 'date'
);
// $recent_posts = wp_get_recent_posts();
$recent_posts = new WP_Query($args);

$idxPost = 0;
if ( $recent_posts->have_posts() ) : 
  echo '<div class="section_vert">'; 
  while ( $recent_posts->have_posts() ) : 
    $recent_posts->the_post();
    if ( $idxPost == 0 ) : ?>
    
<div class="row main">
  <div class="txt_main">
    <span class="glyphicon glyphicon-leaf"></span>
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <span class="date"><?php the_time('Y-m-d'); ?></span>
    <?php the_excerpt(); ?>
  </div>
</div>

<?php
    endif;
    if ( $idxPost == 1 ) echo '<div class="row sub"><ul>';
    if ( $idxPost >= 1 ) : ?>
    <li>
      <span class="glyphicon glyphicon-leaf"></span>
      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      <span class="date"><?php the_time('Y-m-d'); ?></span>
    </li>
<?php
    endif;
    $idxPost++;
  endwhile;
  if ( $idxPost > 1 ) echo '</ul></div>'; 
endif; ?>
